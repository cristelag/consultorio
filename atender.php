<?php
	session_start();
	$conexion=mysqli_connect('localhost', 'medico','', 'consultas');
				if (mysqli_connect_errno()) {
					printf("Conexión fallida %s\n", mysqli_connect_error());
					exit();
				}
	$fecha=$_SESSION['datos'][0];
	$hora=$_SESSION['datos'][1];
	$nombre=$_SESSION['datos'][2];	
	$apellido=$_SESSION['datos'][3];
	$salud=$_SESSION['datos'][4];	
	$nif=$_SESSION['datos'][5];	


?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Atender</title>
	<meta charset="utf-8">	
    <link href="https://fonts.googleapis.com/css?family=Marcellus+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="estilogeneral.css">

	<style>
.formu{
	background-color:white;
	width:500px;
	height:500px;
	padding:30px;
	margin-left:auto;
	margin-right:auto;
	font-size:16px;
	box-shadow: 6px 6px 6px black;


}
input{
	border-radius:5%;
	border: 1px solid grey;
	padding:5px;
	font-family:'Open Sans', sans-serif;

}
select{
	border-radius:5%;
	border: 1px solid grey;
	padding:5px;
	font-family:'Open Sans', sans-serif;

}
legend{
	text-align:center;
}
.asig{
	margin-top:50px;
    background: #0f4c75;
    width: 125px;
    padding-top: 5px;
    padding-bottom: 5px;
    color: white;
    border-radius: 4px;
    border: #3282b8 1px solid;
}
.asig:hover{
	background: #3282b8;
}
h1,h2{
	color:white;
	font-family: 'Marcellus SC', serif;

}



	</style>
</head>
<body>
<h1>Atendiendo al paciente <?php echo $nombre?></h1>
<form action="#" method="POST" class="formu">
<table  style="text-align: center;">
		<tr>
			<th>Paciente</th>
			<td><?php echo $nombre." ".$apellido; ?></td>
        </tr>
		<tr>
			<th>Dni</th>
			<td><?php echo $nif; ?></td>
        </tr>
		<tr>
			<th>Fecha y hora</th>
			<td><?php echo $fecha." ".$hora; ?></td>
        </tr>
		<tr>
			<th>Centro salud</th>
			<td><?php echo $salud; ?></td>
        </tr>

		<tr>
		<th>Observaciones</th>
			<td><textarea name="obs" style="box-sizing: border-box; width: 350px; height: 200px; resize: none; overflow: auto;" ></textarea></td>
		</tr>

</table>

		<button class="asig" type="submit" name="atender" value="Enviar">Enviar</button>
			<button class="asig" type="submit" name="back">Volver</button>
			<button class="asig" type="submit" name="logout">Cerrar Sesión</button>
		</form>
	<?php

	if (isset($_POST['atender'])) {
		$observaciones=$_POST['obs'];
		$sql="UPDATE citas SET citEstado='Atendido', CitObservaciones='$observaciones' WHERE citPaciente='$nif' AND citFecha='$fecha' AND citHora='$hora';";
	
		if (mysqli_query($conexion, $sql)) {
				 echo "<p> Se han registrado las observaciones con éxito</p>";
				
			}
		else {
			echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
		}
	}

	if (isset($_POST['back'])) {

		header("Location:citasPendientes.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:acceso.php");
	}

	mysqli_close($conexion);

	?>
</body>
</html>