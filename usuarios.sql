# Privilegios para `acceso`@`localhost`

GRANT USAGE ON *.* TO 'acceso'@'localhost';

GRANT SELECT ON `consultas`.`usuarios` TO 'acceso'@'localhost';


# Privilegios para `administrador`@`localhost`

GRANT USAGE ON *.* TO 'administrador'@'localhost';

GRANT ALL PRIVILEGES ON `consultas`.* TO 'administrador'@'localhost' WITH GRANT OPTION;


# Privilegios para `asistente`@`localhost`

GRANT USAGE ON *.* TO 'asistente'@'localhost';

GRANT SELECT ON `consultas`.`medicos` TO 'asistente'@'localhost';

GRANT SELECT ON `consultas`.`consultorios` TO 'asistente'@'localhost';

GRANT SELECT, INSERT ON `consultas`.`citas` TO 'asistente'@'localhost';

GRANT SELECT (dniUsu), INSERT ON `consultas`.`usuarios` TO 'asistente'@'localhost';

GRANT SELECT, INSERT ON `consultas`.`pacientes` TO 'asistente'@'localhost';


# Privilegios para `medico`@`localhost`

GRANT USAGE ON *.* TO 'medico'@'localhost';

GRANT SELECT ON `consultas`.`consultorios` TO 'medico'@'localhost';

GRANT SELECT ON `consultas`.`pacientes` TO 'medico'@'localhost';

GRANT SELECT, UPDATE (CitObservaciones, citEstado) ON `consultas`.`citas` TO 'medico'@'localhost';


# Privilegios para `paciente`@`localhost`

GRANT USAGE ON *.* TO 'paciente'@'localhost';

GRANT SELECT (dniMed, medNombres, medApellidos) ON `consultas`.`medicos` TO 'paciente'@'localhost';

GRANT SELECT (citPaciente, citMedico, citFecha, citHora, citEstado, CitObservaciones, citConsultorio) ON `consultas`.`citas` TO 'paciente'@'localhost';

GRANT SELECT ON `consultas`.`consultorios` TO 'paciente'@'localhost';