<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Citas atendidas</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilogeneral.css">
	<link href="https://fonts.googleapis.com/css?family=Marcellus+SC&display=swap" rel="stylesheet">

	<style>
h1,h2{
	color:white;
	font-family: 'Marcellus SC', serif;

}
table{
   margin-left: auto;
	margin-right: auto;
	box-shadow: 6px 6px 6px black;
}
td{
	padding:10px;

}
th{
	
	padding:10px;
}
</style>

</head>
<body><div id="divi">
	
	<?php

	if ($_SESSION['usutipo']=='Medico') {

	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['usuLogin']?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
	<h2>Citas atendidas por <?php echo $_SESSION['usuLogin']; ?></h2>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>

	<?php	
		$conexion=mysqli_connect('localhost', 'medico','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Paciente</th>
			<th>Consultorio</th>
			<th>Observaciones</th>
		</tr>

		<?php

		$nif=$_SESSION['nif'];
		

		$sql="SELECT citas.citFecha, citas.citHora, pacientes.pacNombres, pacientes.pacApellidos, consultorios.conNombre, citas.citObservaciones FROM citas, pacientes, consultorios WHERE citas.citMedico='$nif' AND citas.citEstado='Atendido' AND citas.citPaciente=pacientes.dniPac AND citas.citConsultorio=consultorios.idConsultorio;";
		$resultado = mysqli_query ($conexion, $sql);
				
		$filas=mysqli_num_rows($resultado);
		if ($filas > 0) {
			while ($registro = mysqli_fetch_row($resultado)) {
				
		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]." ".$registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
			<td><?php echo $registro[5]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No hay ninguna cita atendida</td></tr>";
		}

		?>

	</table>

	<?php		

	}

	if ($_SESSION['usutipo']=='Asistente') {

	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>

	<?php	
		$conexion=mysqli_connect('localhost', 'asistente','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Paciente</th>
			<th>Médico</th>
			<th>Consultorio</th>
			<th>Observaciones</th>
		</tr>

		<?php

		$sql="SELECT citas.citFecha,citas.citHora,pacientes.pacNombres,pacientes.pacApellidos,medicos.medNombres,medicos.medApellidos,consultorios.conNombre,citas.citObservaciones FROM citas,pacientes,medicos,consultorios WHERE citas.citEstado='Atendido' AND citas.citPaciente=pacientes.dniPac AND citas.citMedico=medicos.dniMed AND citas.citConsultorio=consultorios.idConsultorio;";
		$resultado = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($resultado);

		if ($filas>0) {
			while ($registro = mysqli_fetch_row($resultado)) {

		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]." ".$registro[3]; ?></td>
			<td><?php echo $registro[4]." ".$registro[5]; ?></td>
			<td><?php echo $registro[6]; ?></td>
			<td><?php echo $registro[7]; ?></td>
		</tr>	

		<?php

			}
		}
		else {
			echo "<tr><td colspan='6'>No hay ninguna cita atendida</td></tr>";
		}

		?>

	</table>	

	<?php	

	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:acceso.php");
	}
	mysqli_close($conexion);
	?>
	</div>
</body>
</html>