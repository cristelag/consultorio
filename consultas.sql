-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-01-2020 a las 13:01:48
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `consultas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `idCita` int(11) NOT NULL,
  `citFecha` date NOT NULL,
  `citHora` time NOT NULL,
  `citPaciente` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `citMedico` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `citConsultorio` int(11) NOT NULL,
  `citEstado` enum('Asignado','Atendido') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Asignado',
  `CitObservaciones` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`idCita`, `citFecha`, `citHora`, `citPaciente`, `citMedico`, `citConsultorio`, `citEstado`, `CitObservaciones`) VALUES
(5, '2019-12-25', '11:00:00', '11111111P', '22222222M', 2, 'Asignado', ''),
(24, '2020-12-30', '16:00:00', '22222222P', '11111111M', 2, 'Atendido', 'Frio horrible'),
(39, '2020-01-15', '12:12:00', '66666666P', '11111111M', 1, 'Atendido', 'Pesadez\r\n'),
(44, '2020-01-27', '11:11:00', '11111111P', '11111111M', 1, 'Atendido', 'F'),
(52, '2020-01-01', '11:11:00', '11111111P', '11111111M', 1, 'Atendido', 'Prueba 1'),
(54, '1221-11-11', '11:11:00', '11111111P', '11111111M', 1, 'Atendido', 'sads'),
(56, '2020-01-24', '12:22:00', '22222222P', '11111111M', 1, 'Atendido', 'bea es una quejica'),
(57, '2020-01-29', '12:22:00', '11111111P', '11111111M', 1, 'Atendido', ''),
(58, '2020-01-28', '12:22:00', '11111111P', '11111111M', 1, 'Atendido', ''),
(59, '2020-01-29', '12:00:00', '11111111P', '11111111M', 1, 'Atendido', ''),
(60, '2020-01-22', '09:30:00', '11111111P', '11111111M', 1, 'Atendido', ''),
(61, '2020-01-30', '10:00:00', '22222222P', '11111111M', 1, 'Atendido', ''),
(62, '2020-01-30', '09:03:00', '22222222P', '11111111M', 1, 'Atendido', ''),
(63, '2020-01-30', '08:59:00', '22222222P', '11111111M', 1, 'Atendido', ''),
(64, '2020-01-23', '08:59:00', '22222222P', '11111111M', 1, 'Atendido', 'r1234r');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultorios`
--

CREATE TABLE `consultorios` (
  `idConsultorio` int(11) NOT NULL,
  `conNombre` char(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `consultorios`
--

INSERT INTO `consultorios` (`idConsultorio`, `conNombre`) VALUES
(1, 'Centro de Salud Oviedo'),
(2, 'Centro de Salud Corvera'),
(3, 'Centro de Salud Aviles'),
(4, 'Centro de Salud Gijon'),
(5, 'Centro de Salud Luarca'),
(6, 'Hospital Universitario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicos`
--

CREATE TABLE `medicos` (
  `dniMed` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `medNombres` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `medApellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `medEspecialidad` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `medTelefono` char(15) COLLATE utf8_spanish_ci NOT NULL,
  `medCorreo` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `medicos`
--

INSERT INTO `medicos` (`dniMed`, `medNombres`, `medApellidos`, `medEspecialidad`, `medTelefono`, `medCorreo`) VALUES
('11111111M', 'Olga', 'Hernandez', 'Cirujana', '689444785', 'olga@medico.zajar'),
('22222222M', 'Carlos', 'Zurita', 'psiquiatria', '633699877', 'zurita@cafe.solo'),
('33333333M', 'Luis', 'Perez', 'psicologia', '647895621', 'luis@fd.es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `dniPac` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `pacNombres` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `pacApellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `pacFechaNacimiento` date NOT NULL,
  `pacSexo` enum('Masculino','Femenino') COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`dniPac`, `pacNombres`, `pacApellidos`, `pacFechaNacimiento`, `pacSexo`) VALUES
('11111111P', 'isaura', 'mendez', '1990-03-07', 'Femenino'),
('22222222P', 'bea', 'berrocal', '1988-09-05', 'Femenino'),
('33333333P', 'cristel', 'alvarez', '1999-02-27', 'Femenino'),
('44444444P', 'Isabel', 'Mendez', '1991-11-19', 'Femenino'),
('66666666P', 'marino', 'rodriguez', '1988-11-16', 'Masculino'),
('88888888P', 'laura', 'melgarejo', '1998-08-04', 'Femenino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `dniUsu` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `usuLogin` char(15) COLLATE utf8_spanish_ci NOT NULL,
  `usuPassword` varchar(157) COLLATE utf8_spanish_ci NOT NULL,
  `usuEstado` enum('Activo','Inactivo') COLLATE utf8_spanish_ci NOT NULL,
  `usutipo` enum('Administrador','Asistente','Medico','Paciente') COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`dniUsu`, `usuLogin`, `usuPassword`, `usuEstado`, `usutipo`) VALUES
('11111111A', 'asis', ' ', 'Activo', 'Asistente'),
('11111111M', 'Olga', ' ', 'Activo', 'Medico'),
('11111111P', 'isaura', ' ', 'Activo', 'Paciente'),
('22222222M', 'Carlos', ' ', 'Activo', 'Medico'),
('22222222P', 'bea', ' ', 'Activo', 'Paciente'),
('33333333M', 'luis', ' ', 'Activo', 'Medico'),
('33333333P', 'cristel', ' ', 'Activo', 'Paciente'),
('44444444P', 'isabel', ' ', 'Activo', 'Paciente'),
('66666666P', 'maro', ' ', 'Activo', 'Paciente'),
('88888888P', 'Laura', ' ', 'Activo', 'Paciente'),
('XXXXXXXXX', 'administrador', ' ', 'Activo', 'Administrador');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`idCita`),
  ADD KEY `citPaciente` (`citPaciente`,`citMedico`,`citConsultorio`),
  ADD KEY `citMedico` (`citMedico`),
  ADD KEY `citConsultorio` (`citConsultorio`);

--
-- Indices de la tabla `consultorios`
--
ALTER TABLE `consultorios`
  ADD PRIMARY KEY (`idConsultorio`);

--
-- Indices de la tabla `medicos`
--
ALTER TABLE `medicos`
  ADD PRIMARY KEY (`dniMed`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`dniPac`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`dniUsu`,`usuLogin`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `idCita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`citPaciente`) REFERENCES `pacientes` (`dniPac`),
  ADD CONSTRAINT `citas_ibfk_2` FOREIGN KEY (`citMedico`) REFERENCES `medicos` (`dniMed`),
  ADD CONSTRAINT `citas_ibfk_3` FOREIGN KEY (`citConsultorio`) REFERENCES `consultorios` (`idConsultorio`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
