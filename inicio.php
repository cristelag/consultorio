<?php

session_start();


?>

<!DOCTYPE html>
<html>
<head>
	<title>Inicio <?php echo $_SESSION['usutipo']; ?></title>
	<link rel="stylesheet" type="text/css" href="estilogeneral.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:700,600' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Marcellus+SC&display=swap" rel="stylesheet">

<style>
#padre{
	display: flex;
  justify-content: center;
  align-items: center;

 
}

h1,h2{
	color:white;
	font-family: 'Marcellus SC', serif;

}
button{
	width:200px;
	height:100px;
}
.button3{
	cursor:pointer;

	color: white !important;
text-transform: uppercase;
font-size:18px;
padding: 20px;
border: 3px solid white !important;
border-radius: 6px;
display: inline-block;
transition: all 0.3s ease 0s;
}
.button3:hover{
color: black !important;
background-color:#bbe1fa;
border-radius: 50px;
border-color: #494949 !important;
transition: all 0.3s ease 0s;

}

</style>
</head>
<body>

	<?php
	$conexion=mysqli_connect('localhost', 'administrador','', 'consultas');
	if (isset($_SESSION['usuLogin']) && isset($_SESSION['usutipo'])) {

		if ($_SESSION['usutipo']=="Administrador") {
			$conexion=mysqli_connect('localhost', 'administrador','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
	<div id="padre">
	<form action="" method="POST">
		
	
			<p><button class="button3" type="submit" name="altap">Alta Paciente</button></p>
			<p><button class="button3" type="submit" name="altam">Alta Médico</button></p>
			<p><button class="button3"type="submit" name="logout">Cerrar Sesión</button></p>
		
	</form>
	</div>

	<?php

			if (isset($_POST['altap'])) {

				header("Location:altap.php");

			}

			if (isset($_POST['altam'])) {

				header("Location:altam.php");

			}

			if (isset($_POST['logout'])) {

				session_destroy();
		 
				header("Location:acceso.php");
			}		

		}

		if ($_SESSION['usutipo']=='Medico') {
			$conexion=mysqli_connect('localhost', 'medico','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="SELECT medNombres,medApellidos FROM medicos WHERE medNombres='".$_SESSION['usuLogin']."';";
			$resultado = mysqli_query ($conexion, $sql);


	?>
	
<h1>Bienvenido/a <?php echo $_SESSION['usuLogin'] ?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
<div id="padre">
	<form action="" method="POST">
		
		
			<p><button  class="button3" type="submit" name="citasa">Ver Citas Atendidas</button></p>
			<p><button  class="button3" type="submit" name="citasp">Ver Citas Pendientes</button></p>
			<p><button  class="button3" type="submit" name="vpac">Ver Pacientes</button></p>
			<p><button  class="button3" type="submit" name="logout">Cerrar Sesión</button></p>
		
	</form>	
	</div>

	<?php

			if (isset($_POST['citasa'])) {

				header("Location:citasAtendidas.php");

			}

			if (isset($_POST['citasp'])) {

				header("Location:citasPendientes.php");

			}

			if (isset($_POST['vpac'])) {

				header("Location:listadoPacientes.php");

			}

			if (isset($_POST['logout'])) {

					session_destroy();
			 
					header("Location:acceso.php");
			}	

		}

		if ($_SESSION['usutipo']=='Asistente') {
			$conexion=mysqli_connect('localhost', 'asistente','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
	<div id="padre">
	<form action="" method="POST">
			<p><button  class="button3" type="submit" name="citasa">Ver Citas Atendidas</button></p>
			<p><button  class="button3" type="submit" name="nuevacita">Nueva Cita</button></p>
			<p><button  class="button3" type="submit" name="altap">Alta Paciente</button></p>
			<p><button class="button3" type="submit" name="vpac">Ver Pacientes</button></p>
			<p><button class="button3" type="submit" name="logout">Cerrar Sesión</button></p>
	
	</form>	
</div>
	<?php

			if (isset($_POST['citasa'])) {

				header("Location:citasAtendidas.php");

			}

			if (isset($_POST['nuevacita'])) {

				header("Location:nuevaCita.php");

			}

			if (isset($_POST['altap'])) {

				header("Location:altap.php");

			}

			if (isset($_POST['vpac'])) {

				header("Location:listadoPacientes.php");

			}

			if (isset($_POST['logout'])) {

					session_destroy();
			 
					header("Location:acceso.php");
			}

		}

		if ($_SESSION['usutipo']=='Paciente') {
			$conexion=mysqli_connect('localhost', 'paciente','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="SELECT pacNombres,pacApellidos FROM pacientes WHERE usuLogin='".$_SESSION['usuLogin']."'";
			$resultado = mysqli_query ($conexion, $sql);


	?>

	<h1>Bienvenido/a <?php echo $_SESSION['usuario'] ?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
	<div id="padre">
	<form action="" method="POST">
		
		
			<p><button class="button3" type="submit" name="vercitas">Ver todas las citas</button></p>
			<p><button class="button3" type="submit" name="logout">Cerrar Sesión</button></p>
	
	</form>	
</div>
	<?php

			if (isset($_POST['vercitas'])) {

				header("Location:verCitas.php");

			}

			if (isset($_POST['logout'])) {

					session_destroy();
			 
					header("Location:acceso.php");
			}

		}
	}
	mysqli_close($conexion);
	?>
	</div>
</body>
</html>