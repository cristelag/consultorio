<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Listado de pacientes </title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilogeneral.css">
	<link href="https://fonts.googleapis.com/css?family=Marcellus+SC&display=swap" rel="stylesheet">

<style>
h1,h2{
	color:white;
	font-family: 'Marcellus SC', serif;

}
table{
   margin-left: auto;
	margin-right: auto;
	box-shadow: 6px 6px 6px black;
}
td{
	padding:10px;

}
th{
	
	padding:10px;
}

</style>
</head>
<body>

	<?php

	if ($_SESSION['usutipo']=='Medico') {

	?>

	<h1>Bienvenido/a <?php echo $_SESSION['usuLogin']?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
	<h2>Listado</h2>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>
	
	<?php

		$conexion=mysqli_connect('localhost', 'medico','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>DNI</th>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Fecha de nacimiento</th>
			<th>Sexo</th>
			
		</tr>

	<?php

	$nif = $_SESSION['nif'];

	$sql="SELECT pacientes.* FROM pacientes,citas WHERE citas.citPaciente=pacientes.dniPac AND citas.citMedico='$nif';";
	$resultado = mysqli_query ($conexion, $sql);
	$filas=mysqli_num_rows($resultado);
	if ($filas>0) {
		while ($registro = mysqli_fetch_row($resultado)) {

		?>
	
		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]; ?></td>
			<td><?php echo $registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No tiene ningún paciente, ".$_SESSION['usuLogin']."</td></tr>";
		}

		?>

	</table>

	<?php					

	}

	if ($_SESSION['usutipo']=='Asistente') {
	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['usutipo'] ?></h1>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>

	<?php	
		$conexion=mysqli_connect('localhost', 'asistente','', 'consultas');
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>DNI</th>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Fecha de nacimiento</th>
			<th>Sexo</th>
		</tr>

		<?php

		$sql="SELECT * FROM pacientes;";
		$resultado = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($resultado);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($resultado)) {

		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]; ?></td>
			<td><?php echo $registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No hay pacientes en el registro</td></tr>";
		}

		?>

	</table>	

	<?php

	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:acceso.php");
	}
	mysqli_close($conexion);

	?>

</body>
</html>