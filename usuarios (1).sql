# Privilegios para `acceso`@`%`

GRANT USAGE ON *.* TO 'acceso'@'%';

GRANT SELECT (usutipo, usuLogin, dniUsu, usuPassword) ON `consultas`.`usuarios` TO 'acceso'@'%';


# Privilegios para `administrador`@`%`

GRANT USAGE ON *.* TO 'administrador'@'%';

GRANT ALL PRIVILEGES ON `consultas`.* TO 'administrador'@'%' WITH GRANT OPTION;


# Privilegios para `asistente`@`%`

GRANT USAGE ON *.* TO 'asistente'@'%';

GRANT SELECT (dniMed, medApellidos, medNombres) ON `consultas`.`medicos` TO 'asistente'@'%';

GRANT SELECT (usuLogin, dniUsu), INSERT ON `consultas`.`usuarios` TO 'asistente'@'%';

GRANT SELECT (citPaciente, citConsultorio, citFecha, citHora, citEstado, citMedico, idCita), INSERT (citPaciente, citConsultorio, citFecha, citHora, citEstado, citMedico, idCita) ON `consultas`.`citas` TO 'asistente'@'%';

GRANT SELECT ON `consultas`.`consultorios` TO 'asistente'@'%';

GRANT SELECT, INSERT ON `consultas`.`pacientes` TO 'asistente'@'%';


# Privilegios para `medico`@`%`

GRANT USAGE ON *.* TO 'medico'@'%';

GRANT SELECT ON `consultas`.`consultorios` TO 'medico'@'%';

GRANT SELECT ON `consultas`.`pacientes` TO 'medico'@'%';

GRANT SELECT, UPDATE (citEstado, CitObservaciones) ON `consultas`.`citas` TO 'medico'@'%';


# Privilegios para `paciente`@`%`

GRANT USAGE ON *.* TO 'paciente'@'%';

GRANT SELECT ON `consultas`.`citas` TO 'paciente'@'%';

GRANT SELECT ON `consultas`.`pacientes` TO 'paciente'@'%';