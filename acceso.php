<!DOCTYPE html>
<html lang="es">
<head>
	<title>Centro Médico</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:700,600' rel='stylesheet' type='text/css'>
	<style>
	
	body{
		
  font-family: 'Open Sans', sans-serif;
  background:linear-gradient(to right,#0f4c75, #3498db,#0f4c75);
  margin: 0 auto 0 auto;  
  width:100%; 
  text-align:center;
  margin: 20px 0px 20px 0px;   
}

p{
  font-size:12px;
  text-decoration: none;
  color:#ffffff;
}

h1{
  font-size:1.5em;
  color:#525252;
}


.box{
  background:white;
  width:300px;
  border-radius:6px;
  margin: 0 auto 0 auto;
  padding:0px 0px 70px 0px;
  border: #2980b9 4px solid; 
}

.email{
  background:#ecf0f1;
  border: #ccc 1px solid;
  border-bottom: #ccc 2px solid;
  padding: 8px;
  width:250px;
  color:#AAAAAA;
  margin-top:10px;
  font-size:1em;
  border-radius:4px;
}

.password{
  border-radius:4px;
  background:#ecf0f1;
  border: #ccc 1px solid;
  padding: 8px;
  width:250px;
  font-size:1em;
}

.btn{
  background:#0f4c75;
  width:125px;
  padding-top:5px;
  padding-bottom:5px;
  color:white;
  border-radius:4px;
  border: #3282b8 1px solid;
  
  margin-top:20px;
  margin-bottom:20px;
  float:left;
  margin-left:16px;
  font-weight:800;
  font-size:0.8em;
}

.btn:hover{
  background:#3282b8; 
}
</style>
</head>
<body>
<div id="caja">
	<center><img src="logo1.png"  alt="Logo de MedicAstur" width="300" height="300"></center>
	<form method="post" action="">
		<div class="box">
			<h1>Inicio de sesión</h1>

		<input type="text" name="usuario"  onFocus="field_focus(this, 'usuario');" onblur="field_blur(this, 'usuario');" class="usuario" required="required"/>
  
<input type="password" name="pass"  onFocus="field_focus(this, 'pass');" onblur="field_blur(this, 'pass');" class="pass" required="required" />
  
<p><input type="submit" class="btn" name="env" value="Entrar"></p>

</div> 
</form>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
	
	<?php

	$conexion = mysqli_connect('localhost', 'acceso', '', 'consultas');
	if (mysqli_connect_errno()) {
	    printf("Conexión fallida %s\n", mysqli_connect_error());
	    exit();
	}

	if (isset($_POST['env'])) {
		$usuario=$_POST['usuario'];
		$password=$_POST['pass'];
		$sql = "SELECT usuLogin,usutipo, dniUsu FROM usuarios WHERE usuLogin = '$usuario' AND usuPassword = '$password'";
		$resultado = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($resultado);
		var_dump($filas);

		if($filas > 0) {
			while ($registro = mysqli_fetch_row($resultado)) {
				$usuLogin=$registro[0];
				$usutipo=$registro[1];
				$nif=$registro[2];
        	}

	        session_start();

	     	$_SESSION['usuLogin']="$usuLogin";
	        $_SESSION['usutipo']="$usutipo";
			$_SESSION['usuario']="$usuario";
			$_SESSION['nif']="$nif";

	        header("Location: inicio.php");
     
        	exit();
		}
		else {
			$mensajeaccesoincorrecto = "<p>El usuario o contraseña no son correctos, vuelva a introducirlos</p>";
        	 echo $mensajeaccesoincorrecto;
		}
	}
	mysqli_close($conexion);

	?>
		<script>
	function field_focus(field, email)
  {
    if(field.value == email)
    {
      field.value = '';
    }
  }

  function field_blur(field, email)
  {
    if(field.value == '')
    {
      field.value = email;
    }
  }

//Fade in dashboard box
$(document).ready(function(){
    $('.box').hide().fadeIn(1000);
    });

//Stop click event
$('a').click(function(event){
    event.preventDefault(); 
	});
	</script>
</body>
</html>